<header class="site-navbar" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-11 col-xl-2">
            <h1 class="mb-0 site-logo"><a href="{{route('welcome')}}" class="text-white h2 mb-0">ChurchApp</a></h1>
          </div>
          <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu js-clone-nav mr-auto d-none d-lg-block">
                <li class="active"><a href="/"><span>Home</span></a></li>
                
                <li><a href="#"><span>About</span></a></li>
                <li><a href="#"><span>Annoucements</span></a></li>
                <li><a href="#"><span>Tech Support</span></a></li>
                <li class="has-children">
                  <a href="#"><span>Account</span></a>
                  <ul class="dropdown arrow-top">
                    <li><a href="{{route('login')}}"><span>login</span></a></li>
                    <!-- <li><a href="{{route('register')}}"><span>User register</span></a></li>
                    <li><a href="{{route('church-reg')}}"><span>Church register</span></a></li> -->
                  </ul>
                </li>
              </ul>
            </nav>
          </div>


          <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>

          </div>

        </div>
      </div>
      
    </header>