
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Church App &mdash; Manage Members, Branches, Home Church, Birthdays</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Rubik:400,700" rel="stylesheet">

    <link rel="stylesheet" href="{{config('app.url')}}app-assets/fonts/icomoon/style.css">

    <link rel="stylesheet" href="{{config('app.url')}}app-assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{config('app.url')}}app-assets/css/magnific-popup.css">
    <link rel="stylesheet" href="{{config('app.url')}}app-assets/css/jquery-ui.css">
    <link rel="stylesheet" href="{{config('app.url')}}app-assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{config('app.url')}}app-assets/css/owl.theme.default.min.css">

    <link rel="stylesheet" href="{{config('app.url')}}app-assets/css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="{{config('app.url')}}app-assets/fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="{{config('app.url')}}app-assets/css/aos.css">
    <link rel="stylesheet" href="{{config('app.url')}}app-assets/css/rangeslider.css">

    <link rel="stylesheet" href="{{config('app.url')}}app-assets/css/style.css">
    <link href="{{config('app.url')}}app-assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    
  </head>

  @include('template.master.header')
  @yield('banner')

  <body>
  
  <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
    
    @yield('content')
    @include('sweetalert::alert')
    @include('template.master.footer')
    @yield('script')
  </div>

  <script src="{{config('app.url')}}app-assets/js/jquery-3.3.1.min.js"></script>
  <script src="{{config('app.url')}}app-assets/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="{{config('app.url')}}app-assets/js/jquery-ui.js"></script>
  <script src="{{config('app.url')}}app-assets/js/popper.min.js"></script>
  <script src="{{config('app.url')}}app-assets/js/bootstrap.min.js"></script>
  <script src="{{config('app.url')}}app-assets/js/owl.carousel.min.js"></script>
  <script src="{{config('app.url')}}app-assets/js/jquery.stellar.min.js"></script>
  <script src="{{config('app.url')}}app-assets/js/jquery.countdown.min.js"></script>
  <script src="{{config('app.url')}}app-assets/js/jquery.magnific-popup.min.js"></script>
  <script src="{{config('app.url')}}app-assets/js/bootstrap-datepicker.min.js"></script>
  <script src="{{config('app.url')}}app-assets/js/aos.js"></script>
  <script src="{{config('app.url')}}app-assets/js/rangeslider.min.js"></script>
  <script src="{{config('app.url')}}app-assets/plugins/select2/js/select2.min.js"></script>
  

  <script src="{{config('app.url')}}app-assets/js/typed.js"></script>
            <script>
            var typed = new Typed('.typed-words', {
            strings: ["Church Members"," Branches"," Home Church", " Birthdays"],
            typeSpeed: 80,
            backSpeed: 80,
            backDelay: 4000,
            startDelay: 1000,
            loop: true,
            showCursor: true
            });
            </script>

  <script src="{{config('app.url')}}app-assets/js/main.js"></script>
  
  </body>
</html>