<div class="left side-menu">
                <div class="slimscroll-menu" id="remove-scroll">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <!-- Left Menu Start -->
                        <ul class="metismenu" id="side-menu">
                            <li class="menu-title">Main</li>
                            <li>
                                <a href="{{route('admin')}}" class="waves-effect">
                                    <i class="ti-home"></i><span class="badge badge-primary badge-pill float-right">2</span> <span> Dashboard </span>
                                </a>
                            </li>
                            <!-- <li>
                                <a href="{{route('church')}}" class="waves-effect"><i class="ti-home"></i><span> Churches </span></a>
                            </li>
                            <li>
                                <a href="{{route('branch')}}" class="waves-effect"><i class="ti-layers-alt"></i><span> Branches </span></a>
                            </li>
                            <li>
                                <a href="{{route('home-cell')}}" class="waves-effect"><i class="ti-layers-alt"></i><span> Home Cell </span></a>
                            </li> -->
                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-user"></i><span> Users </span></a>
                                <ul class="submenu">
                                    <li><a href="{{route('user')}}">All Users</a></li>
                                    
                                </ul>
                            </li>

                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-email"></i><span> Notifications <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                                <ul class="submenu">
                                    <li><a href="{{route('sms-notify')}}">Sms Templates</a></li>
                                    <li><a href="#">Email</a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-settings"></i><span> Settings <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                                <!-- <ul class="submenu">
                                    <li><a href="email-inbox.html">Theme</a></li>
                                    <li><a href="email-read.html">Pages</a></li>
                                    <li><a href="email-compose.html">Others</a></li>
                                </ul> -->
                            </li>

                            

                        </ul>

                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>