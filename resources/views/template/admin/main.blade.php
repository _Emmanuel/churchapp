<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Church App - Send Daily Birthday Wishes | Manage Church, Branch, Members, Home Cell</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Themesbrand" name="author" />
        <link rel="shortcut icon" href="{{config('app.url')}}admin-assets/images/users/dunamis.PNG">

        <!-- DataTables -->
        <link href="{{config('app.url')}}admin-assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="{{config('app.url')}}admin-assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="{{config('app.url')}}admin-assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!--Chartist Chart CSS -->
        <link rel="stylesheet" href="{{config('app.url')}}admin-assets/plugins/chartist/css/chartist.min.css">

        <link href="{{config('app.url')}}admin-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="{{config('app.url')}}admin-assets/css/metismenu.min.css" rel="stylesheet" type="text/css">
        <link href="{{config('app.url')}}admin-assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="{{config('app.url')}}admin-assets/css/style.css" rel="stylesheet" type="text/css">
    </head>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            @include('template.admin.topbar')
            <!-- Top Bar End -->

            <!-- ========== Left Sidebar Start ========== -->
            @include('template.admin.sidebar')
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                @yield('content')
                <!-- content -->
                @include('sweetalert::alert')

                @include('template.admin.footer')

                @yield('script')

            </div>

            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->

        <!-- jQuery  -->
        <script src="{{config('app.url')}}admin-assets/js/jquery.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/js/bootstrap.bundle.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/js/metisMenu.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/js/jquery.slimscroll.js"></script>
        <script src="{{config('app.url')}}admin-assets/js/waves.min.js"></script>

        <!-- Required datatable js -->
        <script src="{{config('app.url')}}admin-assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="{{config('app.url')}}admin-assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/plugins/datatables/jszip.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="{{config('app.url')}}admin-assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/plugins/datatables/buttons.colVis.min.js"></script>
        <!-- Responsive examples -->
        <script src="{{config('app.url')}}admin-assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- Datatable init js -->
        <script src="{{config('app.url')}}admin-assets/pages/datatables.init.js"></script> 

        <!--Chartist Chart-->
        <script src="{{config('app.url')}}admin-assets/plugins/chartist/js/chartist.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/plugins/chartist/js/chartist-plugin-tooltip.min.js"></script>

        <!-- peity JS -->
        <script src="{{config('app.url')}}admin-assets/plugins/peity-chart/jquery.peity.min.js"></script>

        <script src="{{config('app.url')}}admin-assets/pages/dashboard.js"></script>

        <!-- App js -->
        <script src="{{config('app.url')}}admin-assets/js/app.js"></script>
        
    </body>

</html>