
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Church App - Send Daily Birthday Wishes | Manage Church, Branch, Members, Home Cell</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Themesbrand" name="author" />
        <link rel="shortcut icon" href="{{config('app.url')}}admin-assets/images/favicon.ico">

        <link href="{{config('app.url')}}admin-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="{{config('app.url')}}admin-assets/css/metismenu.min.css" rel="stylesheet" type="text/css">
        <link href="{{config('app.url')}}admin-assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="{{config('app.url')}}admin-assets/css/style.css" rel="stylesheet" type="text/css">
    </head>

    <body>

        <div class="home-btn d-none d-sm-block">
                <a href="{{route('welcome')}}" class="text-dark"><i class="fas fa-home h2"></i></a>
            </div>
            
            <div class="wrapper-page">
    
                <div class="card overflow-hidden account-card mx-3">
                    
                    <div class="bg-primary p-4 text-white text-center position-relative">
                        <h4 class="font-20 m-b-5">Register</h4>
                        <p class="text-white-50 mb-4">Get your dunamis account now.</p>
                        <a href="{{route('welcome')}}" class="logo logo-admin"><img src="{{config('app.url')}}admin-assets/images/logo-sm.png" height="24" alt="logo"></a>
                    </div>
                    <div class="account-card-content">  
    
                        <form method="POST" action="{{ route('register') }}" class="form-horizontal m-t-30">
                        @csrf

                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus id="username" placeholder="Enter username">
                                @error('name')
                                    <span style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            
                            <div class="form-group">
                                <label for="useremail">Email</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email" id="useremail" placeholder="Enter email">
                                @error('email')
                                    <span style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="userpassword">Password</label>
                                <input type="password" class="form-control" name="password" required autocomplete="new-password" id="userpassword" placeholder="Enter password">
                                @error('password')
                                    <span style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password-confirm">Confirm Password</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            
                            </div>    

                            <div class="form-group row m-t-20">
                                <div class="col-12 text-right">
                                    <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Register</button>
                                </div>
                            </div>

                            <div class="form-group m-t-10 mb-0 row">
                                <div class="col-12 m-t-20">
                                    <p class="mb-0">By registering you agree to the Church <a href="#" class="text-primary">Vision, Mission & Terms of service</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
    
                <div class="m-t-40 text-center">
                    <p>Already have an account ? <a href="{{route('login')}}" class="font-500 text-primary"> Login </a> </p>
                    <p>© 2021 Dunamis Int'l. Crafted with <i class="mdi mdi-heart text-danger"></i> by Team TechIntellects</p>
                </div>
    
            </div>
            <!-- end wrapper-page -->

        <!-- jQuery  -->
        <script src="{{config('app.url')}}admin-assets/js/jquery.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/js/bootstrap.bundle.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/js/metisMenu.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/js/jquery.slimscroll.js"></script>
        <script src="{{config('app.url')}}admin-assets/js/waves.min.js"></script>

        <!-- App js -->
        <script src="{{config('app.url')}}admin-assets/js/app.js"></script>

    </body>

</html>