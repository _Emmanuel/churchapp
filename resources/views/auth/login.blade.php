
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Church App - Send Daily Birthday Wishes | Manage Church, Branch, Members, Home Cell</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Themesbrand" name="author" />
        <link rel="shortcut icon" href="{{config('app.url')}}admin-assets/images/favicon.ico">

        <link href="{{config('app.url')}}admin-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="{{config('app.url')}}admin-assets/css/metismenu.min.css" rel="stylesheet" type="text/css">
        <link href="{{config('app.url')}}admin-assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="{{config('app.url')}}admin-assets/css/style.css" rel="stylesheet" type="text/css">
    </head>

    <body>

        <div class="home-btn d-none d-sm-block">
            <a href="/" class="text-dark"><i class="fas fa-home h2"></i></a>
        </div>
        
        <div class="wrapper-page">
        <form method="POST" action="{{ route('login') }}">
                        @csrf
            <div class="card overflow-hidden account-card mx-3">
                
                <div class="bg-primary p-4 text-white text-center position-relative">
                    <h4 class="font-20 m-b-5">Welcome Back !</h4>
                    <p class="text-white-50 mb-4">Sign in to continue.</p>
                    <a href="/" class="logo logo-admin"><img src="{{config('app.url')}}admin-assets/images/logo-sm.png" height="24" alt="logo"></a>
                </div>
                <div class="account-card-content"> 

                        <div class="form-group">
                            <label for="username">Email</label>
                            <input type="email" class="form-control"  name="email" value="{{ old('email') }}" required autocomplete="email" autofocus id="username" placeholder="Enter Email">
                            @error('email')
                                    <span style="color:red">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group">
                            <label for="userpassword">Password</label>
                            <input type="password" class="form-control" name="password" required autocomplete="current-password" id="userpassword" placeholder="Enter password">
                                @error('password')
                                    <span style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group row m-t-20">
                            <div class="col-sm-6">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customControlInline" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="customControlInline">Remember me</label>
                                </div>
                            </div>
                            <div class="col-sm-6 text-right">
                                <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Log In</button>
                            </div>
                        </div>

                        <div class="form-group m-t-10 mb-0 row">
                            <div class="col-12 m-t-20">
                            @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                                <!-- <a href="pages-recoverpw.html"><i class="mdi mdi-lock"></i> Forgot your password?</a> -->
                            </div>
                        </div>
                </div>
            </div>

            <div class="m-t-40 text-center">
                <p>Don't have an account ? <a href="{{route('register')}}" class="font-500 text-primary"> Signup now </a> </p>
                <p>© 2021 Dunamis Int'l. Crafted with <i class="mdi mdi-heart text-danger"></i> by Team Tech Intellects</p>
            </div>
            </form>
        </div>
        <!-- end wrapper-page -->


        <!-- jQuery  -->
        <script src="{{config('app.url')}}admin-assets/js/jquery.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/js/bootstrap.bundle.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/js/metisMenu.min.js"></script>
        <script src="{{config('app.url')}}admin-assets/js/jquery.slimscroll.js"></script>
        <script src="{{config('app.url')}}admin-assets/js/waves.min.js"></script>

        <!-- App js -->
        <script src="{{config('app.url')}}admin-assets/js/app.js"></script>

    </body>

</html>