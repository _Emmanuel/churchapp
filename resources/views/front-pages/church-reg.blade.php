
 
@extends('template.master.main')

@section('content')

    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url({{config('app.url')}}app-assets/images/hero_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">

          <div class="col-md-10" data-aos="fade-up" data-aos-delay="400">
            
            
            <div class="row justify-content-center">
              <div class="col-md-8 text-center">
                <h1>Register your church</h1>
                <p data-aos="fade-up" data-aos-delay="100">Complete the form to register your church, you would receive a verification email, your church would be listed once verified.</p>
              </div>
            </div>

            
          </div>
        </div>
      </div>
    </div>  

    <div class="site-section bg-light">
      <div class="container">
        <div class="row">
          <div class="col-md-6 mb-5">

            <form action="{{route('add_church')}}" method="POST" class="p-5 bg-white" style="margin-top: -150px;">
            @csrf             

              
              <a>Church Details</a>
              <div class="row form-group">
              
                <div class="col-md-6 mb-3 mb-md-0">
                  <label class="text-black" for="fname">Church Name</label>
                  <input required type="text" name="name" id="fname" class="form-control">
                </div>
                <div class="col-md-6">
                  <label class="text-black" for="lname">Church Address</label>
                  <input required type="text" name="address" id="lname" class="form-control">
                </div>
              </div>


              <div class="row form-group">
              <div class="col-md-6 mb-3 mb-md-0">
                  <label class="text-black" for="fname">Founder</label>
                  <input required type="text" name="founder" id="fname" class="form-control">
                </div>
                <div class="col-md-6">
                  <label class="text-black" for="lname">Date Founded</label>
                  <input required type="text" name="date_founded" id="lname" class="form-control">
                </div>
                </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="email">Email</label> 
                  <input required type="email" name="email" id="email" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="phones">Phone</label> 
                  <input required type="phone" name="phones" id="phone" class="form-control">
                </div>
              </div>              
              

              <div class="row form-group">
                <div class="col-md-12">
                  <input type="submit" value="Submit" class="btn btn-primary btn-md text-white">
                </div>
              </div>

  
            </form>

            
          </div>
          <div class="col-md-6 mb-5">

            <!--<form action="#" class="p-5 bg-white" style="margin-top: -150px;">
             

              <div class="row form-group">
                <div class="col-md-6 mb-3 mb-md-0">
                  <label class="text-black" for="fname">First Name</label>
                  <input type="text" id="fname" class="form-control">
                </div>
                <div class="col-md-6">
                  <label class="text-black" for="lname">Last Name</label>
                  <input type="text" id="lname" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="email">Email</label> 
                  <input type="email" id="email" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="pass1">Password</label> 
                  <input type="password" id="pass1" class="form-control">
                </div>
              </div>
              
              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="pass2">Re-type Password</label> 
                  <input type="password" id="pass2" class="form-control">
                </div>
              </div>
              

              <div class="row form-group">
                <div class="col-md-12">
                  <input type="submit" value="Sign Up" class="btn btn-primary btn-md text-white">
                </div>
              </div>

  
            </form> -->
          </div>
        </div>
      </div>
    </div>
@endsection
