@extends('template.admin.main')

@section('content')
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        
                        <!-- end row -->

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">

                                        <h4 class="mt-0 header-title">Manage Branch</h4>
                                      <!--  <p class="text-muted m-b-30">DataTables has most features enabled by
                                            default, so all you need to do to use it with your own tables is to call
                                            the construction function: <code>$().DataTable();</code>.
                                        
                                        </p> -->

                                        <div class="col-sm-6 col-md-3 m-t-30">
                                                <div class="text-right">
                                                    <p class="text-muted"></p>
                                                    <!-- Small modal -->
                                                    <button type="button" class="btn btn-primary waves-effect waves-light mdi mdi-clipboard-plus" id="add-branch-modal" data-toggle="modal" data-target=".bs-example-modal-center"> Add Branch</button>
                                                </div>
        
                                                <div class="modal fade bs-example-modal-center" id="add-branch-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title mt-0">Add to Branch</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                    <div class="modal-body">
                                    <div class="card">                        
                                    <div class="card-body">
        
                                        <h4 class="mt-0 header-title">Enter branch details to add branch</h4>
                                        <form action="{{route('add_branch')}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="name" value="I dont Know">
                                        <div class="form-group row">
                                            <label class="col-sm-5 col-form-label">Select Church</label>
                                            <div class="col-sm-7">
                                                <select class="form-control" name="church_id" required>
                                                    <option value="">--Select--</option>
                                                    @foreach($churches as $church)
                                                    <option value="{{$church->id}}">{{$church->name}}</option>
                                                    @endforeach
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-5 col-form-label">Location Name</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="location" type="text" placeholder="Enter church location" id="example-text-input">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-5 col-form-label">Address</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="address" type="text" placeholder="Branch Address" id="example-search-input">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-5 col-form-label">Resident Pastor</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="resident_pastor" type="text" placeholder="Resident Pastor's Name" id="example-text-input">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-email-input" class="col-sm-5 col-form-label">Email</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="email" type="email" placeholder="Admin or Pastor's Email" id="example-url-input">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-tel-input" class="col-sm-5 col-form-label">Phones</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="phones" type="tel" placeholder="Church admin or Pastors Number" id="example-tel-input">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Date Founded</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="date_founded" type="date" >
                                            </div>
                                        </div>
                                        <div class="button-items">
                                            <button class="btn btn-success waves-effect waves-light" type="submit">Submit</button>
                                        </div>
                                        </form>
                                        
                                        
                                        
                                    </div>
                                </div>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                            </div>

                                        
                                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <!--<th>Church</th> -->
                                                <th>Location</th>
                                                <th>Resident Pastor</th>
                                                <th>Email</th>
                                                <th>Phones</th>
                                                <th>Address</th>
                                                <th>Date Founded</th>
                                                <th>Action</th>
                                                
                                                
                                               <!-- <th>Status</th> -->
                                               
                                            </tr>
                                            </thead>


                                            <tbody>

                                            @forelse($branches as $branch)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                               <!-- <td>{{$branch->church->name}}</td> -->
                                                <td>{{$branch->location}}</td>
                                                <td>{{$branch->resident_pastor}}</td>
                                                <td>{{$branch->phones}}</td>
                                                <td>{{$branch->email}}</td>
                                                <td>{{$branch->address}}</td>
                                                <td>{{$branch->date_founded}}</td>
                                                <td><div>
                                                        <ul class="list-inline menu-left mb-0">
                                                        <li class="d-none d-sm-block">
                                                            <div class="dropdown pt-3 d-inline-block">
                                                                <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Manage Branch
                                                                </a>
                                                                
                                                                <!-- Branch Update Button -->
                                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                                    <a class="dropdown-item" type="button" class="btn btn-warning waves-effect waves-light">View </a>

                                                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#updatebranch-{{$branch->id}}">Update</a>



                                                                    <a class="dropdown-item" href="">Delete</a>
                                                                  <!--  <a class="dropdown-item" href="#">Something else here</a> -->
                                                                    <div class="dropdown-divider"></div>
                                                                    <a class="dropdown-item" href="#">Manage Departments</a>
                                                                    <a class="dropdown-item" href="#">Manage Members</a>
                                                                    <a class="dropdown-item" href="{{route('home-cell')}}">Manage HomeCell</a>
                                                                    <a class="dropdown-item" href="#">Manage Admin</a>
                                                                </div>
                                                                
                                                            </div>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                                
                                                <!--<td><span class="badge badge-warning">Pending</span></td> -->
                                                
                                            
                                            </tr>
                                            @empty
                                            <p> There is no branch</p>
                                            @endforelse
                                            </tbody>
                                        </table>
                                        <!-- Edit Branch modal -->
                                        



                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->
                                            

                        


                    </div>
                    <!-- container-fluid -->                
 
                  <!-- content -->
@endsection

@section('script')
  @foreach($branches as $branch)
    @include('admin-pages.update_branch')
  @endforeach
@endsection




