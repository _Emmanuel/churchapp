
@extends('template.admin.main')

@section('content')
<div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                
                                <div class="col-sm-6">
                                    <h4 class="page-title">Hello {{Auth()->user()->name}}</h4>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item active">Welcome to church Dashboard</li>
                                    </ol>

                                </div>
                                <div class="col-sm-6">
                                
                                    <div class="float-right d-none d-md-block">
                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle arrow-none waves-effect waves-light" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="mdi mdi-settings mr-2"></i> Settings
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#">Action</a>
                                                <!-- <a class="dropdown-item" href="#">Another action</a>
                                                <a class="dropdown-item" href="#">Something else here</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#">Separated link</a> -->
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="{{config('app.url')}}admin-assets/images/services-icon/01.png" alt="" >
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">First Timers</h5>
                                            <h4 class="font-500">1,685 <i class="mdi mdi-arrow-up text-success ml-2"></i></h4>
                                            <div class="mini-stat-label bg-success">
                                                <p class="mb-0">+ 12%</p>
                                            </div>
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>
        
                                            <p class="text-white-50 mb-0">Since last month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="{{config('app.url')}}admin-assets/images/services-icon/02.png" alt="" >
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">New Converts</h5>
                                            <h4 class="font-500">52,368 <i class="mdi mdi-arrow-up text-danger ml-2"></i></h4>
                                            <div class="mini-stat-label bg-danger">
                                                <p class="mb-0">+ 28%</p>
                                            </div>
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>
        
                                            <p class="text-white-50 mb-0">Since last month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="{{config('app.url')}}admin-assets/images/services-icon/03.png" alt="" >
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">New Members</h5>
                                            <h4 class="font-500">15.8 <i class="mdi mdi-arrow-up text-success ml-2"></i></h4>
                                            <div class="mini-stat-label bg-info">
                                                <p class="mb-0"> 00%</p>
                                            </div>
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>
        
                                            <p class="text-white-50 mb-0">Since last month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="{{config('app.url')}}admin-assets/images/services-icon/04.png" alt="" >
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">New Locations</h5>
                                            <h4 class="font-500">436 <i class="mdi mdi-arrow-up text-success ml-2"></i></h4>
                                            <div class="mini-stat-label bg-warning">
                                                <p class="mb-0">+ 84%</p>
                                            </div>
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>
        
                                            <p class="text-white-50 mb-0">Since last month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        
                        <!-- end row -->

                        <div class="row">
                            <div class="col-xl-8">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="mt-0 header-title mb-4">Recent Birthdays & Anniversaries</h4>
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                  <tr>
                                                    <th scope="col">(#) Id</th>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Phone</th>
                                                    <th scope="col" colspan="2"> SMS Status</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <th scope="row">#14256</th>
                                                    <td>
                                                        <div>
                                                            <img src="{{config('app.url')}}admin-assets/images/users/user-2.jpg" alt="" class="thumb-md rounded-circle mr-2"> Philip Smead
                                                        </div>
                                                    </td>
                                                    <td>15/1/2021</td>
                                                    <td>08123456947</td>
                                                    <td><span class="badge badge-success">Delivered</span></td>
                                                    <td>
                                                        <div>
                                                            <a href="#" class="btn btn-primary btn-sm">Edit</a>
                                                        </div>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <th scope="row">#14257</th>
                                                    <td>
                                                        <div>
                                                            <img src="{{config('app.url')}}admin-assets/images/users/user-3.jpg" alt="" class="thumb-md rounded-circle mr-2"> Brent Shipley
                                                        </div>
                                                    </td>
                                                    <td>16/03/2021</td>
                                                    <td>08123456947</td>
                                                    <td><span class="badge badge-warning">Pending</span></td>
                                                    <td>
                                                        <div>
                                                            <a href="#" class="btn btn-primary btn-sm">Edit</a>
                                                        </div>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <th scope="row">#14258</th>
                                                    <td>
                                                        <div>
                                                            <img src="{{config('app.url')}}admin-assets/images/users/user-4.jpg" alt="" class="thumb-md rounded-circle mr-2"> Robert Sitton
                                                        </div>
                                                    </td>
                                                    <td>17/1/2021</td>
                                                    <td>08123456947</td>
                                                    <td><span class="badge badge-success">Delivered</span></td>
                                                    <td>
                                                        <div>
                                                            <a href="#" class="btn btn-primary btn-sm">Edit</a>
                                                        </div>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <th scope="row">#14259</th>
                                                    <td>
                                                        <div>
                                                            <img src="{{config('app.url')}}admin-assets/images/users/user-5.jpg" alt="" class="thumb-md rounded-circle mr-2"> Alberto Jackson
                                                        </div>
                                                    </td>
                                                    <td>18/1/2021</td>
                                                    <td>08123456947</td>
                                                    <td><span class="badge badge-danger">Cancel</span></td>
                                                    <td>
                                                        <div>
                                                            <a href="#" class="btn btn-primary btn-sm">Edit</a>
                                                        </div>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <th scope="row">#14260</th>
                                                    <td>
                                                        <div>
                                                            <img src="{{config('app.url')}}admin-assets/images/users/user-6.jpg" alt="" class="thumb-md rounded-circle mr-2"> David Sanchez
                                                        </div>
                                                    </td>
                                                    <td>02/2/2021</td>
                                                    <td>08123456947</td>
                                                    <td><span class="badge badge-success">Delivered</span></td>
                                                    <td>
                                                        <div>
                                                            <a href="#" class="btn btn-primary btn-sm">Edit</a>
                                                        </div>
                                                    </td>
                                                  </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="mt-0 header-title mb-4">Upcoming birthdays & anniversaries</h4>
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                  <tr>
                                                    <th scope="col">(#) Id</th>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Phone</th>
                                                    <th scope="col" colspan="2">Status</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <th scope="row">#14256</th>
                                                    <td>
                                                        <div>
                                                            <img src="{{config('app.url')}}admin-assets/images/users/user-2.jpg" alt="" class="thumb-md rounded-circle mr-2"> Philip Smead
                                                        </div>
                                                    </td>
                                                    <td>15/3/2021</td>
                                                    <td>08123456947</td>
                                                    <td><span class="badge badge-success">Delivered</span></td>
                                                    <td>
                                                        <div>
                                                            <a href="#" class="btn btn-primary btn-sm">Edit</a>
                                                        </div>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <th scope="row">#14257</th>
                                                    <td>
                                                        <div>
                                                            <img src="{{config('app.url')}}admin-assets/images/users/user-3.jpg" alt="" class="thumb-md rounded-circle mr-2"> Brent Shipley
                                                        </div>
                                                    </td>
                                                    <td>16/04/2021</td>
                                                    <td>08123456947</td>
                                                    <td><span class="badge badge-warning">Pending</span></td>
                                                    <td>
                                                        <div>
                                                            <a href="#" class="btn btn-primary btn-sm">Edit</a>
                                                        </div>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <th scope="row">#14258</th>
                                                    <td>
                                                        <div>
                                                            <img src="{{config('app.url')}}admin-assets/images/users/user-4.jpg" alt="" class="thumb-md rounded-circle mr-2"> Robert Sitton
                                                        </div>
                                                    </td>
                                                    <td>17/2/2021</td>
                                                    <td>08123456947</td>
                                                    <td><span class="badge badge-success">Delivered</span></td>
                                                    <td>
                                                        <div>
                                                            <a href="#" class="btn btn-primary btn-sm">Edit</a>
                                                        </div>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <th scope="row">#14259</th>
                                                    <td>
                                                        <div>
                                                            <img src="{{config('app.url')}}admin-assets/images/users/user-5.jpg" alt="" class="thumb-md rounded-circle mr-2"> Alberto Jackson
                                                        </div>
                                                    </td>
                                                    <td>18/3/2021</td>
                                                    <td>08123456947</td>
                                                    <td><span class="badge badge-danger">Cancel</span></td>
                                                    <td>
                                                        <div>
                                                            <a href="#" class="btn btn-primary btn-sm">Edit</a>
                                                        </div>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <th scope="row">#14260</th>
                                                    <td>
                                                        <div>
                                                            <img src="{{config('app.url')}}admin-assets/images/users/user-6.jpg" alt="" class="thumb-md rounded-circle mr-2"> David Sanchez
                                                        </div>
                                                    </td>
                                                    <td>19/4/2021</td>
                                                    <td>08123456947</td>
                                                    <td><span class="badge badge-success">Delivered</span></td>
                                                    <td>
                                                        <div>
                                                            <a href="#" class="btn btn-primary btn-sm">Edit</a>
                                                        </div>
                                                    </td>
                                                  </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- container-fluid -->

                </div>
@endsection
