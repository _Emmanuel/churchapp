
                                                    <!-- Small modal -->

                                                    <!-- Modal -->
    <div id="updateuser-{{$user->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog">
                                                    
                                                    
        
                                                
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">Update User Profile</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                                    <div class="modal-body">
                                        <div class="card">                        
                                            <div class="card-body">
                
                                                <h4 class="mt-0 header-title">Edit user profile</h4>
                                                <form action="{{route('update-user')}}" method="POST">
                                                @csrf
                                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                                <div class="form-group row">
                                                    <label class="col-sm-5 col-form-label">Select User Type</label>
                                                    <div class="col-sm-7">
                                                        <select class="form-control" name="user_type_id" required>
                                                            <option value="{{$user->user_type_id}}">-Select-</option>
                                                            <option value="1"> Pastor</option>
                                                            <option value="2">Others</option>

                                                            <!-- <option value="{{$user->user_type_id}}">{{$user->user_type() ?? "--Select--"}}</option> -->
                                                            <!-- @foreach($users as $user)
                                                            <option value="{{$user->user_type_id}}">{{$user->user_type()}}</option>
                                                            @endforeach -->
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-5 col-form-label"> Name</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="name" name="name" type="text" value="{{$user->name}}" id="example-text-input">
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="example-email-input" class="col-sm-5 col-form-label">Location</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="current_location" name="current_location" type="text" value="{{$user->current_location}}" id="example-text-input">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-tel-input" class="col-sm-5 col-form-label">Phones</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="phones" name="phones" type="tel" value="{{$user->phones}}" id="example-tel-input">
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Date of Birth</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="birthday" name="birthday" type="date" value="{{$user->date_of_birth}}" >
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-5 col-form-label"> Wedding Anniversary</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="wed_anniversary" name="wed_anniversary" type="date" value="{{$user->wed_anniversary}}" id="example-text-input">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-email-input" class="col-sm-5 col-form-label">Spouse Name</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="spouse_name" name="spouse_name" type="text" value="{{$user->spouse_name}}" id="example-text-input">
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group row">
                                                    <label for="example-email-input" class="col-sm-5 col-form-label">Spouse Birthday</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="spouse_d_o_b" name="spouse_d_o_b" type="date" value="{{$user->spouse_d_o_b}}" id="example-text-input">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-tel-input" class="col-sm-5 col-form-label">Spouse Phone Number</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="spouse_phone" name="spouse_phone" type="tel" value="{{$user->spouse_phone}}" id="example-tel-input">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Email</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="email" name="email" type="email" value="{{$user->email}}">
                                                    </div>
                                                </div>
                                                    <div class="form-group row">
                                                        <label for="example-datetime-local-input" class="col-sm-5 col-form-label">State</label>
                                                        <div class="col-sm-7">
                                                            <select  id="State" name="State" class="form-control" required>
                                                                <option value="{{$user->State}}">- Select -</option>
                                                                <option value="Abuja FCT">Abuja FCT</option>
                                                                <option value="Abia">Abia</option>
                                                                <option value="Adamawa">Adamawa</option>
                                                                <option value="Akwa Ibom">Akwa Ibom</option>
                                                                <option value="Anambra">Anambra</option>
                                                                <option value="Bauchi">Bauchi</option>
                                                                <option value="Bayelsa">Bayelsa</option>
                                                                <option value="Benue">Benue</option>
                                                                <option value="Borno">Borno</option>
                                                                <option value="Cross River">Cross River</option>
                                                                <option value="Delta">Delta</option>
                                                                <option value="Ebonyi">Ebonyi</option>
                                                                <option value="Edo">Edo</option>
                                                                <option value="Ekiti">Ekiti</option>
                                                                <option value="Enugu">Enugu</option>
                                                                <option value="Gombe">Gombe</option>
                                                                <option value="Imo">Imo</option>
                                                                <option value="Jigawa">Jigawa</option>
                                                                <option value="Kaduna">Kaduna</option>
                                                                <option value="Kano">Kano</option>
                                                                <option value="Katsina">Katsina</option>
                                                                <option value="Kebbi">Kebbi</option>
                                                                <option value="Kogi">Kogi</option>
                                                                <option value="Kwara">Kwara</option>
                                                                <option value="Lagos">Lagos</option>
                                                                <option value="Nassarawa">Nassarawa</option>
                                                                <option value="Niger">Niger</option>
                                                                <option value="Ogun">Ogun</option>
                                                                <option value="Ondo">Ondo</option>
                                                                <option value="Osun">Osun</option>
                                                                <option value="Oyo">Oyo</option>
                                                                <option value="Plateau">Plateau</option>
                                                                <option value="Rivers">Rivers</option>
                                                                <option value="Sokoto">Sokoto</option>
                                                                <option value="Taraba">Taraba</option>
                                                                <option value="Yobe">Yobe</option>
                                                                <option value="Zamfara">Zamfara</option>
                                                                <option value="Outside Nigeria">Outside Nigeria</option>
                                    
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Region</label>
                                                        <div class="col-sm-7">
                                                            <!-- <input class="form-control" id="region" name="region" type="text" value="{{$user->region}}" > -->

                                                            <select  id="region" name="region" class="form-control" required>
                                                            
                                                            <option value="{{$user->region}}">- Select -</option>
                                                                    <option value="Abuja FCT">Abuja FCT</option>
                                                                    <option value="North Central">North Central</option>
                                                                    <option value="South West">South West</option>
                                                                    <option value="South East">South East</option>
                                                                    <option value="North East">North East</option>
                                                                    <option value="South South">South South</option>
                                                                <option value="Outside Nigeria">Outside Nigeria</option>
                                                        </select>


                                                    </div>
                                                </div>
                                                
                                                <div class="button-items">
                                                    <button class="btn btn-success waves-effect waves-light" type="submit">Submit</button>
                                                </div>
                                                </form>
                                                
                                                
                                                
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                        </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
                                                