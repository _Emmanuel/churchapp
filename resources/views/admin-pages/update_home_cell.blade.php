<!-- Modal Update home_cell-->
<div id="updatehome_cell-{{$home_cell->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"></button>
          <h4 class="modal-title">Update home_cell</h4>
        </div>
        <div class="modal-body">
        
                                <h4 class="mt-0 header-title">Enter Home Cell details to add New Home Church</h4>
                                <form action="{{route('update-home-cell')}}" method="POST">
                                @csrf
                                
                                <!-- <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-5 col-form-label">Branch Name</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="name" type="text" placeholder="Enter Church Branch location" id="example-text-input">
                                    </div>
                                </div> -->
                                <div class="form-group row">
                                            <label class="col-sm-5 col-form-label">Select Church Branch</label>
                                            <div class="col-sm-7">
                                                <select class="form-control" name="branch_id" required>
                                                    <option value="">--Select--</option>
                                                    @foreach($branches ?? '' as $branch)
                                                    <option value="{{$branch->id}}">{{$branch->location}}</option>
                                                    @endforeach
                                                    
                                                </select>
                                            </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-5 col-form-label">Home Church Name</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="cell_name" type="text" value="{{$home_cell->cell_name}}" id="example-text-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-5 col-form-label">Cell Location</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="cell_location" type="text" value="{{$home_cell->cell_location}}" id="example-text-input">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-tel-input" class="col-sm-5 col-form-label">Cell Host</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="cell_host" type="text" value="{{$home_cell->cell_host}}" id="example-text-input">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-email-input" class="col-sm-5 col-form-label">Cell Leader</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="cell_leader" type="text" value="{{$home_cell->cell_leader}}" id="example-text-input">
                                    </div>
                                </div>
                                
                                
                                <div class="form-group row">
                                    <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Cell Address</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="cell_address" type="text" value="{{$home_cell->cell_address}}" id="example-text-input">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Phones</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="phones" type="text" value="{{$home_cell->phones}}" id="example-text-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Email</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="email" type="email" value="{{$home_cell->email}}" id="example-text-input">
                                    </div>
                                </div>
                                <!-- <input type="hidden" name="cell_date_founded" type="cell_date_founded" value="I dont Know"> -->

                                
                                <div class="button-items">
                                    <button class="btn btn-success waves-effect waves-light" type="submit">Submit</button>
                                </div>
                                </form>
                            </div>



        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
  
    </div>
  </div>

        
                                        
                            