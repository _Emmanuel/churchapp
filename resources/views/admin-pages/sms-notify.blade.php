@extends('template.admin.main')

@section('content')
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        
                        <!-- end row -->

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                    <!-- <h4 class="page-title">Hello {{Auth()->user()->name}}</h4> -->
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item active">Hello <strong>{{Auth()->user()->name}}</strong>! Welcome to Dashboard, Manage SMS Templates</li>
                                        </ol>

                                        <!-- <h4 class="mt-0 header-title">SMS Templates</h4> -->
                                      <!--  <p class="text-muted m-b-30">DataTables has most features enabled by
                                            default, so all you need to do to use it with your own tables is to call
                                            the construction function: <code>$().DataTable();</code>.
                                        
                                        </p> -->

                                <div class="col-sm-6 col-md-3 m-t-30">
                                                <div class="text-right">
                                                    <p class="text-muted"></p>
                                                    <!-- Small modal -->
                                                    <button type="button" class="btn btn-primary waves-effect waves-light mdi mdi-clipboard-plus" id="add-branch-modal" data-toggle="modal" data-target=".bs-example-modal-center"> Add New Text</button>
                                                    
                                                </div>
        
                                                <div class="modal fade bs-example-modal-center" id="add-branch-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title mt-0">Add New Draft</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                    <div class="modal-body">
                                    <div class="card">                        
                                    <div class="card-body">
        
                                        <h4 class="mt-0 header-title">Select Message Type & Add New Draft</h4>
                                        
                                        @csrf
                                        <input type="hidden" name="name" value="I dont Know">
                                        <div class="form-group row">
                                            <label class="col-sm-5 col-form-label">Select User</label>
                                            <div class="col-sm-7">
                                                <select class="form-control" name="sms_notifies_id" required>
                                                    <option value="">--Select--</option>
                                                    @foreach($sms_notifies as $sms_notify)
                                                    <option value="{{$sms_notify->id}}">{{$sms_notify->message_type}}</option>
                                                    @endforeach
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-5 col-form-label"> Message</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="message" type="textarea" placeholder="Type your message here..." id="example-text-input">
                                            </div>
                                        </div>
                                        
                                        
                                       
                                        <div class="button-items">
                                            <button class="btn btn-success waves-effect waves-light" type="submit">Submit</button>
                                        </div>
                                        </form>
                                        
                                        
                                        
                                    </div>
                                </div>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                            </div>
                                        <!-- List Users from users table-->
                                        
                                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Message Type</th>
                                                <th>Text</th>
                                                <th>Action</th>
                                                
                                                
                                               <!-- <th>Status</th> -->
                                               
                                            </tr>
                                            </thead>


                                            <tbody>

                                            @forelse($sms_notifies as $sms_notify)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$sms_notify->message_type}}</td>
                                                <td>{{$sms_notify->message}}</td>
                                                <td><div>
                                                        <ul class="list-inline menu-left mb-0">
                                                        <li class="d-none d-sm-block">
                                                            <div class="dropdown pt-3 d-inline-block">
                                                                <a class="btn btn-warning dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Edit
                                                                </a>
                                                            </div>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                                
                                                <!--<td><span class="badge badge-warning">Pending</span></td> -->
                                                
                                            
                                            </tr>
                                            @empty
                                            <p> No Draft</p>
                                            @endforelse
                                            </tbody>
                                        </table>
                                        <!-- Edit Branch modal -->
                                        



                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->
                                            

                        


                    </div>
                    <!-- container-fluid -->                
 
                  <!-- content -->
@endsection






