
@extends('template.admin.main')

@section('content')
<div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                
                                <div class="col-sm-6">
                                    <h4 class="page-title">Dashboard</h4>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item active">Welcome to Veltrix Dashboard</li>
                                    </ol>

                                </div>
                                <div class="col-sm-6">
                                
                                    <div class="float-right d-none d-md-block">
                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle arrow-none waves-effect waves-light" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="mdi mdi-settings mr-2"></i> Settings
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#">Action</a>
                                                <a class="dropdown-item" href="#">Another action</a>
                                                <a class="dropdown-item" href="#">Something else here</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#">Separated link</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="{{config('app.url')}}admin-assets/images/services-icon/01.png" alt="" >
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">Orders</h5>
                                            <h4 class="font-500">1,685 <i class="mdi mdi-arrow-up text-success ml-2"></i></h4>
                                            <div class="mini-stat-label bg-success">
                                                <p class="mb-0">+ 12%</p>
                                            </div>
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>
        
                                            <p class="text-white-50 mb-0">Since last month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="{{config('app.url')}}admin-assets/images/services-icon/02.png" alt="" >
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">Revenue</h5>
                                            <h4 class="font-500">52,368 <i class="mdi mdi-arrow-down text-danger ml-2"></i></h4>
                                            <div class="mini-stat-label bg-danger">
                                                <p class="mb-0">- 28%</p>
                                            </div>
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>
        
                                            <p class="text-white-50 mb-0">Since last month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="{{config('app.url')}}admin-assets/images/services-icon/03.png" alt="" >
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">Average Price</h5>
                                            <h4 class="font-500">15.8 <i class="mdi mdi-arrow-up text-success ml-2"></i></h4>
                                            <div class="mini-stat-label bg-info">
                                                <p class="mb-0"> 00%</p>
                                            </div>
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>
        
                                            <p class="text-white-50 mb-0">Since last month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="{{config('app.url')}}admin-assets/images/services-icon/04.png" alt="" >
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">Product Sold</h5>
                                            <h4 class="font-500">2436 <i class="mdi mdi-arrow-up text-success ml-2"></i></h4>
                                            <div class="mini-stat-label bg-warning">
                                                <p class="mb-0">+ 84%</p>
                                            </div>
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>
        
                                            <p class="text-white-50 mb-0">Since last month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        
                        <!-- end row -->

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">

                                        <h4 class="mt-0 header-title">All Home Cell</h4>
                                        <p class="text-muted m-b-30">The Buttons extension for DataTables
                                            provides a common set of options, API methods and styling to display
                                            buttons on a page that will interact with a DataTable. The core library
                                            provides the based framework upon which plug-ins can built.
                                        </p>     
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="button" class="btn btn-primary waves-effect waves-light mdi mdi-clipboard-plus" data-toggle="modal" data-target=".bs-example-modal-center">Add Home Church</button>
                        </div>
                        <br>
                        
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            
                                <thead>
                                
                                
                                <tr>
                                    <th>S/N </th>
                                    <th>Branch Name</th>
                                    <th>Home Cell Name</th>
                                    <th>Location</th>
                                    <th>Host</th>                                               
                                    <th>Leader</th>
                                    <th>Phones</th>
                                    <th>Date Founded</th>
                                    <th>Action</th>
                                    <th>Address</th>
                                </tr>
                                </thead>

                                <tbody>

                                @forelse($home_cells as $home_cell)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$home_cell->branch->name}}</td>
                                    <td>{{$home_cell->cell_name}}</td>
                                    <td>{{$home_cell->cell_location}}</td>
                                    <td>{{$home_cell->cell_host}}</td>
                                    <td>{{$home_cell->cell_leader}}</td>
                                    <td>{{$home_cell->phones}}</td>
                                    <td>{{$home_cell->cell_date_founded}}</td>
                                    <td><div>
                                            <a href="#" class="btn btn-primary btn-sm">Manage</a>
                                            <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#updatehome_cell-{{$home_cell->id}}">Edit</a>
                                            
                                        </div>
                                    </td>
                                    <td>{{$home_cell->cell_address}}</td>
                                    
                                </tr>
                                @empty
                                <p>No Home Cell Found!</p>
                                @endforelse
                                
                                </tbody>
                            </table>
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- container-fluid -->

                </div>

                <!-- Small modal -->
                
                                                

                <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">Add Home Church</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
        
                                <h4 class="mt-0 header-title">Enter Home Cell details to add New Home Church</h4>
                                <form action="#" method="POST">
                                @csrf
                                
                                <!-- <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-5 col-form-label">Branch Name</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="name" type="text" placeholder="Enter Church Branch location" id="example-text-input">
                                    </div>
                                </div> -->
                                <div class="form-group row">
                                            <label class="col-sm-5 col-form-label">Select Church Branch</label>
                                            <div class="col-sm-7">
                                                <select class="form-control" name="branch_id" required>
                                                    <option value="">--Select--</option>
                                                    @foreach($branches ?? '' as $branch)
                                                    <option value="{{$branch->id}}">{{$branch->location}}</option>
                                                    @endforeach
                                                    
                                                </select>
                                            </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-5 col-form-label">Home Church Name</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="cell_name" type="text" placeholder="Cell Name" id="example-text-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-5 col-form-label">Cell Location</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="cell_location" type="text" placeholder="HomeCell Location" id="example-text-input">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-tel-input" class="col-sm-5 col-form-label">Cell Host</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="cell_host" type="text" placeholder="Cell Host" id="example-text-input">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-email-input" class="col-sm-5 col-form-label">Cell Leader</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="cell_leader" type="text" placeholder="Cell Leader" id="example-text-input">
                                    </div>
                                </div>
                                
                                
                                <div class="form-group row">
                                    <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Cell Address</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="cell_address" type="text" placeholder="Cell Address" id="example-text-input">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Phones</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="phones" type="text" placeholder="Phones" id="example-text-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Email</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="email" type="email" placeholder="Email" id="example-text-input">
                                    </div>
                                </div>
                                <!-- <input type="hidden" name="cell_date_founded" type="cell_date_founded" value="I dont Know"> -->

                                
                                <div class="button-items">
                                    <button class="btn btn-success waves-effect waves-light" type="submit">Submit</button>
                                </div>
                                </form>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
@endsection

@section('script')
  @foreach($home_cells as $home_cell)
    @include('admin-pages.update_home_cell')
  @endforeach
@endsection