@extends('template.admin.main')

@section('content')
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        
                        <!-- end row -->

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">

                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item active">Hello <strong>{{Auth()->user()->name}}</strong>! Welcome to Dashboard, Manage SMS Templates</li>
                                        </ol>

                                        <!-- <h4 class="mt-0 header-title">Manage Users</h4> -->
                                      <!--  <p class="text-muted m-b-30">DataTables has most features enabled by
                                            default, so all you need to do to use it with your own tables is to call
                                            the construction function: <code>$().DataTable();</code>.
                                        
                                        </p> -->

                                            <div class="col-sm-6 col-md-3 m-t-30">
                                                <div class="text-right">
                                                    <p class="text-muted"></p>
                                                    <!-- Small modal -->
                                                        <button type="button" class="btn btn-primary waves-effect waves-light mdi mdi-clipboard-plus" id="add-user-modal" data-toggle="modal" data-target=".bs-example-modal-center"> Add a User</button>
                                                            <a class="btn btn-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Upload Users
                                                            </a>
                                                </div>
        
                                                <div class="modal fade bs-example-modal-center" id="add-user-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title mt-0">Add to Users</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                                <div class="modal-body">
                                                                    <div class="card">                        
                                                                        <div class="card-body">
                                    
                                                                            <h4 class="mt-0 header-title">Enter user details to add user</h4>
                                                                            <form action="{{route('add_user')}}" method="POST">
                                                                            @csrf
                                                                            <input type="hidden" name="name" value="I dont Know">
                                                                            <div class="form-group row">
                                                                                <label class="col-sm-5 col-form-label">Select User Type</label>
                                                                                <div class="col-sm-7">
                                                                                    <select class="form-control" name="user_type" required>
                                                                                        <option value="">--Select--</option>
                                                                                        <option value="1">Pastor</option>
                                                                                        <option value="2">Member</option>                                                                                       
                                                                                        
                                                                                        
                                                                                        
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label for="example-text-input" class="col-sm-5 col-form-label"> Name</label>
                                                                                <div class="col-sm-7">
                                                                                    <input class="form-control" name="name" type="text" placeholder="Firstname Surname" id="example-text-input">
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="form-group row">
                                                                                <label for="example-email-input" class="col-sm-5 col-form-label">Location</label>
                                                                                <div class="col-sm-7">
                                                                                    <input class="form-control" name="current_location" type="text" placeholder="Current Branch Location" id="example-text-input">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label for="example-tel-input" class="col-sm-5 col-form-label">Phones</label>
                                                                                <div class="col-sm-7">
                                                                                    <input class="form-control" name="phones" type="tel" placeholder="Phone Number" id="example-tel-input">
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="form-group row">
                                                                                <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Date of Birth</label>
                                                                                <div class="col-sm-7">
                                                                                    <input class="form-control" name="birthday" type="date" >
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label for="example-text-input" class="col-sm-5 col-form-label"> Wedding Anniversary</label>
                                                                                <div class="col-sm-7">
                                                                                    <input class="form-control" name="wed_anniversary" type="date" id="example-text-input">
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label for="example-email-input" class="col-sm-5 col-form-label">Spouse Name</label>
                                                                                <div class="col-sm-7">
                                                                                    <input class="form-control" name="spouse_name" type="text" placeholder="Enter Spouse Name" id="example-text-input">
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            
                                                                            <div class="form-group row">
                                                                                <label for="example-email-input" class="col-sm-5 col-form-label">Spouse Birthday</label>
                                                                                <div class="col-sm-7">
                                                                                    <input class="form-control" name="spouse_d_o_b" type="date" placeholder="Day and Month e.g 30-02" id="example-text-input">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label for="example-tel-input" class="col-sm-5 col-form-label">Spouse Phone Number</label>
                                                                                <div class="col-sm-7">
                                                                                    <input class="form-control" name="spouse_phone" type="tel" placeholder="Spouse Phone Number" id="example-tel-input">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Email</label>
                                                                                <div class="col-sm-7">
                                                                                    <input class="form-control" name="email" type="text" placeholder="name@dunamisgospel.org">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label for="example-datetime-local-input" class="col-sm-5 col-form-label">State</label>
                                                                                <div class="col-sm-7">
                                                                                <!-- <input class="form-control" name="State" type="text" placeholder="Branch State" > -->
                                                                                    <select  name="State" class="form-control" required>
                                                                                    
                                                                                        <option value="">- Select -</option>
                                                                                            <option value="Abuja FCT">Abuja FCT</option>
                                                                                            <option value="Abia">Abia</option>
                                                                                            <option value="Adamawa">Adamawa</option>
                                                                                            <option value="Akwa Ibom">Akwa Ibom</option>
                                                                                            <option value="Anambra">Anambra</option>
                                                                                            <option value="Bauchi">Bauchi</option>
                                                                                            <option value="Bayelsa">Bayelsa</option>
                                                                                            <option value="Benue">Benue</option>
                                                                                            <option value="Borno">Borno</option>
                                                                                            <option value="Cross River">Cross River</option>
                                                                                            <option value="Delta">Delta</option>
                                                                                            <option value="Ebonyi">Ebonyi</option>
                                                                                            <option value="Edo">Edo</option>
                                                                                            <option value="Ekiti">Ekiti</option>
                                                                                            <option value="Enugu">Enugu</option>
                                                                                            <option value="Gombe">Gombe</option>
                                                                                            <option value="Imo">Imo</option>
                                                                                            <option value="Jigawa">Jigawa</option>
                                                                                            <option value="Kaduna">Kaduna</option>
                                                                                            <option value="Kano">Kano</option>
                                                                                            <option value="Katsina">Katsina</option>
                                                                                            <option value="Kebbi">Kebbi</option>
                                                                                            <option value="Kogi">Kogi</option>
                                                                                            <option value="Kwara">Kwara</option>
                                                                                            <option value="Lagos">Lagos</option>
                                                                                            <option value="Nassarawa">Nassarawa</option>
                                                                                            <option value="Niger">Niger</option>
                                                                                            <option value="Ogun">Ogun</option>
                                                                                            <option value="Ondo">Ondo</option>
                                                                                            <option value="Osun">Osun</option>
                                                                                            <option value="Oyo">Oyo</option>
                                                                                            <option value="Plateau">Plateau</option>
                                                                                            <option value="Rivers">Rivers</option>
                                                                                            <option value="Sokoto">Sokoto</option>
                                                                                            <option value="Taraba">Taraba</option>
                                                                                            <option value="Yobe">Yobe</option>
                                                                                            <option value="Zamfara">Zamfara</option>
                                                                                        <option value="Outside Nigeria">Outside Nigeria</option>
                                                            
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Region</label>
                                                                                    <div class="col-sm-7">
                                                                                        <!-- <input class="form-control" name="region" type="text" placeholder="Region e.g North, SouthWest, NorthEast " > -->
                                                                                            <select  id="region" name="region" class="form-control" required>
                                                                                        
                                                                                                <option value="">- Select -</option>
                                                                                                <option value="Abuja FCT">Abuja FCT</option>
                                                                                                <option value="North Central">North Central</option>
                                                                                                <option value="South West">South West</option>
                                                                                                <option value="South East">South East</option>
                                                                                                <option value="North East">North East</option>
                                                                                                <option value="South South">South South</option>
                                                                                                <option value="Outside Nigeria">Outside Nigeria</option>
                                                                                            </select>
                                                                                    </div>
                                                                            </div>
                                                                            
                                                                            <div class="button-items">
                                                                                <button class="btn btn-success waves-effect waves-light" type="submit">Submit</button>
                                                                            </div>
                                                                            </form>
                                                                    
                                                                    
                                                                    
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                    </div><!-- /.modal -->
                                                </div>
                                            </div>
                                        <!-- List Users from users table-->
                                        
                                        <table id="datatable-buttons" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                
                                                <th>Name</th>
                                                <th>Location</th>
                                                <th>Phones</th>
                                                <th>Birthdays</th>
                                                <th>Anniversary</th>
                                                <th>Spouse Name</th>
                                                <th>Spouse D-O-B</th>
                                                <th>Spouse Phone</th>
                                                <th>Email</th>
                                                <th>State</th>
                                                <th>Region</th>
                                                <th>User Type</th>
                                                <th>Action</th>
                                                
                                                
                                               <!-- <th>Status</th> -->
                                               
                                            </tr>
                                            </thead>


                                            <tbody>

                                            @forelse($users as $user)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->current_location}}</td>
                                                <td>{{$user->phones}}</td>
                                                <td>{{$user->birthday}}</td>
                                                <td>{{$user->wed_anniversary}}</td>
                                                <td>{{$user->spouse_name}}</td>
                                                <td>{{$user->spouse_d_o_b}}</td>
                                                <td>{{$user->spouse_phone}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->State}}</td>
                                                <td>{{$user->region}}</td>
                                                <td>{{$user->user_type()}}</td>
                                                <td><div>
                                                        <ul class="list-inline menu-left mb-0">
                                                        <li class="d-none d-sm-block">
                                                            <div class="dropdown pt-3 d-inline-block">
                                                                <!-- <a class="btn btn-warning dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Edit
                                                                </a> -->
                                                                <a href="#" class="dropdown-item" data-toggle="modal" data-target="#updateuser-{{$user->id}}">Edit</a>
                                                                
                                                                
                                                                
                                                            </div>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                                
                                                <!--<td><span class="badge badge-warning">Pending</span></td> -->
                                                
                                            
                                            </tr>
                                            @empty
                                            <p> No User</p>
                                            @endforelse
                                            </tbody>
                                        </table>
                                        <!-- Edit Branch modal -->
                                        



                                    </div> <!-- End Card Body -->
                                </div> <!--end Card -->
                            </div> <!-- end col -->
                        </div> <!-- end row -->

                    </div><!-- container-fluid -->                
 
                </div><!-- content -->
@endsection
@section('script')
    @foreach($users as $user)
        @include('admin-pages.update_user')
    @endforeach
 
@endsection





