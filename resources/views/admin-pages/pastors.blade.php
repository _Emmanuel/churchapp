@extends('template.admin.main')

@section('content')
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        
                        <!-- end row -->

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">

                                        <h4 class="mt-0 header-title">Manage Users</h4>
                                      <!--  <p class="text-muted m-b-30">DataTables has most features enabled by
                                            default, so all you need to do to use it with your own tables is to call
                                            the construction function: <code>$().DataTable();</code>.
                                        
                                        </p> -->

                                        <div class="col-sm-6 col-md-3 m-t-30">
                                                <div class="text-right">
                                                    <p class="text-muted"></p>
                                                    <!-- Small modal -->
                                                    <button type="button" class="btn btn-primary waves-effect waves-light mdi mdi-clipboard-plus" id="add-branch-modal" data-toggle="modal" data-target=".bs-example-modal-center"> Add a User</button>
                                                    <br><a class="btn btn-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Upload Users
                                                                </a>
                                                </div>
        
                                                <div class="modal fade bs-example-modal-center" id="add-branch-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title mt-0">Add to Users</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                    <div class="modal-body">
                                    <div class="card">                        
                                    <div class="card-body">
        
                                        <h4 class="mt-0 header-title">Enter user details to add user</h4>
                                        <!-- <form action="{{route('user')}}" method="POST"> -->
                                        @csrf
                                        <input type="hidden" name="name" value="I dont Know">
                                        <div class="form-group row">
                                            <label class="col-sm-5 col-form-label">Select User</label>
                                            <div class="col-sm-7">
                                                <select class="form-control" name="user_id" required>
                                                    <option value="">--Select--</option>
                                                    @foreach($errors as $user)
                                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                                    @endforeach
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-5 col-form-label"> Name</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="name" type="text" placeholder="Firstname Surname" id="example-text-input">
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="form-group row">
                                            <label for="example-email-input" class="col-sm-5 col-form-label">Email</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="email" type="email" placeholder="Email" id="example-url-input">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-tel-input" class="col-sm-5 col-form-label">Phones</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="phones" type="tel" placeholder="Phone Number" id="example-tel-input">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Date of Birth</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="date_of_birth" type="date" >
                                            </div>
                                        </div>
                                        <div class="button-items">
                                            <button class="btn btn-success waves-effect waves-light" type="submit">Submit</button>
                                        </div>
                                        </form>
                                        
                                        
                                        
                                    </div>
                                </div>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                            </div>
                                        <!-- List Users from users table-->
                                        
                                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phones</th>
                                                <th>Birthdays</th>
                                                <th>User Type</th>
                                                <th>Action</th>
                                                
                                                
                                               <!-- <th>Status</th> -->
                                               
                                            </tr>
                                            </thead>


                                            <tbody>

                                            @forelse($pastors as $pastor)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->phones}}</td>
                                                <td>{{$user->birthday}}</td>
                                                <td>{{$user->user_type()}}</td>
                                                <td><div>
                                                        <ul class="list-inline menu-left mb-0">
                                                        <li class="d-none d-sm-block">
                                                            <div class="dropdown pt-3 d-inline-block">
                                                                <a class="btn btn-warning dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Edit
                                                                </a>
                                                                
                                                                
                                                                
                                                            </div>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                                
                                                <!--<td><span class="badge badge-warning">Pending</span></td> -->
                                                
                                            
                                            </tr>
                                            @empty
                                            <p> No User</p>
                                            @endforelse
                                            </tbody>
                                        </table>
                                        <!-- Edit Branch modal -->
                                        



                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->
                                            

                        


                    </div>
                    <!-- container-fluid -->                
 
                  <!-- content -->
@endsection






