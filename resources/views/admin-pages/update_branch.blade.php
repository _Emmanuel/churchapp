{{-- <div class="col-sm-6 col-md-3 m-t-30">
                                                <div class="text-center">
                                                    <p class="text-muted">-</p>
                                                    <!-- Large modal -->
                                                    
                                                </div>
        
        
                                                <!--  Modal content for the above example -->
    <div class="modal fade bs-example-modal-lg" id="edit-branch-{{$branch->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-right modal-success modal-lg">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title mt-0" id="myLargeModalLabel">Update Branch</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  </div>
                  <div class="modal-body">
                  <div class="card-body">

                  <h4 class="mt-0 header-title">Edit branch details to update branch</h4>
                  <form action="{{route('update-branch')}}" method="POST">
                  @csrf
                  <input type="hidden" name="branch_id" value="{{$branch->id}}">
                  <div class="form-group row">
                      <label class="col-sm-5 col-form-label">Select Church</label>
                      <div class="col-sm-7">
                          <select class="form-control" id="church_id" name="church_id" required>
                          <option value="{{$branch->church_id}}">{{$branch->church->name ?? "--Select--"}}</option>
                              @foreach($churches as $church)
                              <option value="{{$church->id}}">{{$church->name}}</option>
                              @endforeach
                              
                          </select>
                      </div>
                  </div>
                  
                  <div class="form-group row">
                      <label for="example-text-input" class="col-sm-5 col-form-label">Location Name</label>
                      <div class="col-sm-7">
                          <input class="form-control" id="location" name="location" type="text" value="{{$branch->location}}" id="example-text-input">
                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="example-text-input" class="col-sm-5 col-form-label">Address</label>
                      <div class="col-sm-7">
                          <input class="form-control" id="address" name="address" type="text" value="{{$branch->address}}" id="example-search-input">
                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="example-text-input" class="col-sm-5 col-form-label">Resident Pastor</label>
                      <div class="col-sm-7">
                          <input class="form-control" id="resident_pastor" name="resident_pastor" type="text" value="{{$branch->resident_pastor ?? "N/A"}}" id="example-text-input">
                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="example-email-input" class="col-sm-5 col-form-label">Email</label>
                      <div class="col-sm-7">
                          <input class="form-control" id="email" name="email" type="email" value="{{$branch->email ?? "N/A"}}" id="example-url-input">
                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="example-tel-input" class="col-sm-5 col-form-label">Phones</label>
                      <div class="col-sm-7">
                          <input class="form-control" id="phones" name="phones" type="tel" value="{{$branch->phones ?? "N/A"}}" id="example-tel-input">
                      </div>
                  </div>
                  
                  <div class="form-group row">
                      <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Date Founded</label>
                      <div class="col-sm-7">
                          <input class="form-control" id="date_founded" name="date_founded" value="{{$branch->date_founded ?? "N/A"}}" type="date" >
                      </div>
                  </div>
                  <div class="button-items">
                      <button class="btn btn-success waves-effect waves-light" type="submit">Save Changes</button>
                  </div>
                  </form>



                      </div>
                  </div>
              </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
                                            </div> --}}


<!-- Modal -->
    <div id="updatebranch-{{$branch->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"></button>
          <h4 class="modal-title">Update Branch</h4>
        </div>
        <div class="modal-body">

                <form action="{{route('update-branch')}}" method="POST">
                @csrf
                <input type="hidden" name="branch_id" value="{{$branch->id}}">
                <div class="form-group row">
                    <label class="col-sm-5 col-form-label">Select Church</label>
                    <div class="col-sm-7">
                        <select class="form-control" id="church_id" name="church_id" required>
                        <option value="{{$branch->church_id}}">{{$branch->church->name ?? "--Select--"}}</option>
                            @foreach($churches as $church)
                            <option value="{{$church->id}}">{{$church->name}}</option>
                            @endforeach
                            
                        </select>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-5 col-form-label">Location Name</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="location" name="location" type="text" value="{{$branch->location}}" id="example-text-input">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-5 col-form-label">Address</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="address" name="address" type="text" value="{{$branch->address}}" id="example-search-input">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-5 col-form-label">Resident Pastor</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="resident_pastor" name="resident_pastor" type="text" value="{{$branch->resident_pastor ?? "N/A"}}" id="example-text-input">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-email-input" class="col-sm-5 col-form-label">Email</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="email" name="email" type="email" value="{{$branch->email ?? "N/A"}}" id="example-url-input">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-tel-input" class="col-sm-5 col-form-label">Phones</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="phones" name="phones" type="tel" value="{{$branch->phones ?? "N/A"}}" id="example-tel-input">
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Date Founded</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="date_founded" name="date_founded" value="{{$branch->date_founded ?? "N/A"}}" type="date" >
                    </div>
                </div>
                <div class="button-items">
                    <button class="btn btn-success waves-effect waves-light" type="submit">Save Changes</button>
                </div>
                </form>
          


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
  
    </div>
  </div>