
@extends('template.admin.main')

@section('content')
<div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                
                                <div class="col-sm-6">
                                    <h4 class="page-title">Dashboard</h4>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item active">Welcome to Veltrix Dashboard</li>
                                    </ol>

                                </div>
                                <div class="col-sm-6">
                                
                                    <div class="float-right d-none d-md-block">
                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle arrow-none waves-effect waves-light" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="mdi mdi-settings mr-2"></i> Settings
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#">Action</a>
                                                <a class="dropdown-item" href="#">Another action</a>
                                                <a class="dropdown-item" href="#">Something else here</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#">Separated link</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="{{config('app.url')}}admin-assets/images/services-icon/01.png" alt="" >
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">Orders</h5>
                                            <h4 class="font-500">1,685 <i class="mdi mdi-arrow-up text-success ml-2"></i></h4>
                                            <div class="mini-stat-label bg-success">
                                                <p class="mb-0">+ 12%</p>
                                            </div>
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>
        
                                            <p class="text-white-50 mb-0">Since last month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="{{config('app.url')}}admin-assets/images/services-icon/02.png" alt="" >
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">Revenue</h5>
                                            <h4 class="font-500">52,368 <i class="mdi mdi-arrow-down text-danger ml-2"></i></h4>
                                            <div class="mini-stat-label bg-danger">
                                                <p class="mb-0">- 28%</p>
                                            </div>
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>
        
                                            <p class="text-white-50 mb-0">Since last month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="{{config('app.url')}}admin-assets/images/services-icon/03.png" alt="" >
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">Average Price</h5>
                                            <h4 class="font-500">15.8 <i class="mdi mdi-arrow-up text-success ml-2"></i></h4>
                                            <div class="mini-stat-label bg-info">
                                                <p class="mb-0"> 00%</p>
                                            </div>
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>
        
                                            <p class="text-white-50 mb-0">Since last month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="{{config('app.url')}}admin-assets/images/services-icon/04.png" alt="" >
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">Product Sold</h5>
                                            <h4 class="font-500">2436 <i class="mdi mdi-arrow-up text-success ml-2"></i></h4>
                                            <div class="mini-stat-label bg-warning">
                                                <p class="mb-0">+ 84%</p>
                                            </div>
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>
        
                                            <p class="text-white-50 mb-0">Since last month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        
                        <!-- end row -->

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">

                                        <h4 class="mt-0 header-title">All Churches</h4>
                                        <p class="text-muted m-b-30">The Buttons extension for DataTables
                                            provides a common set of options, API methods and styling to display
                                            buttons on a page that will interact with a DataTable. The core library
                                            provides the based framework upon which plug-ins can built.
                                        </p>
                                        <!--Modal form & Button -->
                                        <div class="col-sm-6 col-md-3 m-t-30">
                                                <div class="text-right">
                                                    <p class="text-muted"></p>
                                                    <!-- Small modal -->
                                                    <button type="button" class="btn btn-primary waves-effect waves-light mdi mdi-clipboard-plus" data-toggle="modal" data-target=".bs-example-modal-center"> Add Church</button>
                                                </div>
        
                                                <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title mt-0">Add Church</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="card">
                                    <div class="card-body">
        
                                        <h4 class="mt-0 header-title">Enter Church details to add branch</h4>
                                        <form action="{{route('add_church')}}" method="POST">
                                        @csrf
                                        
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-5 col-form-label">Church Name</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="name" type="text" value="Enter church location" id="example-text-input">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-5 col-form-label">Founder</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="founder" type="text" value="Church Founder" id="example-text-input">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-5 col-form-label">Church Address</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="address" type="text" value="Church Address" id="example-text-input">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-5 col-form-label">Location</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="location" type="text" value="Church Location" id="example-text-input">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="example-tel-input" class="col-sm-5 col-form-label">Phones</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="phones" type="tel" value="Church admin or Pastors Number" id="example-tel-input">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="example-email-input" class="col-sm-5 col-form-label">Email</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="email" type="email" value="Admin or Pastor's Email" id="example-url-input">
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="form-group row">
                                            <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Date Founded</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" name="date_founded" type="date" >
                                            </div>
                                        </div>
                                        <div class="button-items">
                                            <button class="btn btn-success waves-effect waves-light" type="submit">Submit</button>
                                        </div>
                                        </form>
                                        
                                        
                                        
                                    </div>
                                    
                                </div> <!--Modal end-->
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                            <tr>
                                                <th>S/N </th>
                                                <th>Name</th>
                                                <th>Founder</th>
                                                <th>Address</th>
                                                <th>Location</th>
                                                <th>Phones</th>                                               
                                                <th>Email</th>
                                                <th>Start date</th>
                                                <th>Action</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>


                                            <tbody>

                                            @forelse($churches as $church)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$church->name}}</td>
                                                <td>{{$church->founder}}</td>
                                                <td>{{$church->address}}</td>
                                                <td>{{$church->location}}</td>
                                                <td>{{$church->phones}}</td>
                                                <td>{{$church->email}}</td>
                                                <td>{{$church->date_founded}}</td>
                                                <td><div>
                                                        <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#updatechurch-{{$church->id}}">Edit</a>
                                                    </div>
                                                    <div>
                                                    <a href="{{route('church-branch',$church->slug)}}" class="btn btn-primary btn-sm">Branches</a>
                                                    </div>
                                                </td>
                                                <td><span class="badge badge-warning">Pending</span></td>
                                                
                                            </tr>
                                            @empty
                                            <p>No Church Found!</p>
                                            @endforelse
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- container-fluid -->

                </div>
@endsection

@section('script')
  @foreach($churches as $church)
    @include('admin-pages.update_church')
  @endforeach
@endsection