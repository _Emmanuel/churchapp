<!-- Modal Update Church-->
<div id="updatechurch-{{$church->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"></button>
          <h4 class="modal-title">Update Church</h4>
        </div>
        <div class="modal-body">
        <h4 class="mt-0 header-title">Edit church details to Update branch</h4>
                <form action="{{route('update-church')}}" method="POST">
                @csrf
                <input type="hidden" name="church_id" value="{{$church->id}}">
                
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-5 col-form-label">Church Name</label>
                    <div class="col-sm-7">
                        <input class="form-control" name="name" type="text" value="{{$church->name ?? "N/A" }}" id="example-text-input">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-5 col-form-label">Founder</label>
                    <div class="col-sm-7">
                        <input class="form-control" name="founder" type="text" value="{{$church->founder}}" id="example-text-input">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-5 col-form-label">Church Address</label>
                    <div class="col-sm-7">
                        <input class="form-control" name="address" type="text" value="{{$church->address}}" id="example-text-input">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="example-tel-input" class="col-sm-5 col-form-label">Phones</label>
                    <div class="col-sm-7">
                        <input class="form-control" name="phones" type="tel" value="{{$church->phones}}" id="example-tel-input">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="example-email-input" class="col-sm-5 col-form-label">Email</label>
                    <div class="col-sm-7">
                        <input class="form-control" name="email" type="email" value="{{$church->email}}" id="example-url-input">
                    </div>
                </div>
                
                
                <div class="form-group row">
                    <label for="example-datetime-local-input" class="col-sm-5 col-form-label">Date Founded</label>
                    <div class="col-sm-7">
                        <input class="form-control" name="date_founded" value="{{$church->date_founded}}" type="date" >
                    </div>
                </div>

                <div class="button-items">
                    <button class="btn btn-success waves-effect waves-light" type="submit">Submit</button>
                </div>
                </form>
        </div>



        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
  
    </div>
  </div>

        
                                        
                            