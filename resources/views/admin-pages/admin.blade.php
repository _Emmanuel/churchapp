
@extends('template.admin.main')

@section('content')
<div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                
                                <div class="col-sm-6">
                                    <h4 class="page-title">Hello {{Auth()->user()->name}}</h4>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item active">Welcome to church Dashboard</li>
                                    </ol>

                                </div>
                                <div class="col-sm-6">
                                
                                    <div class="float-right d-none d-md-block">
                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle arrow-none waves-effect waves-light" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="mdi mdi-settings mr-2"></i> Settings
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#">Action</a>
                                                <!-- <a class="dropdown-item" href="#">Another action</a>
                                                <a class="dropdown-item" href="#">Something else here</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#">Separated link</a> -->
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="{{config('app.url')}}admin-assets/images/services-icon/01.png" alt="" >
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">Pastors</h5>
                                            <h4 class="font-500">{{$users->count()}} <i class="mdi mdi-arrow-up text-success ml-2"></i></h4>
                                            <div class="mini-stat-label bg-success">
                                                <p class="mb-0">+ 12%</p>
                                            </div>
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>
        
                                            <p class="text-white-50 mb-0">Since last month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="{{config('app.url')}}admin-assets/images/services-icon/02.png" alt="" >
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">Message Sent</h5>
                                            <h4 class="font-500">{{$send_wish->count()}}<i class="mdi mdi-arrow-up text-danger ml-2"></i></h4>
                                            <div class="mini-stat-label bg-danger">
                                                <p class="mb-0">+ 28%</p>
                                            </div>
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>
        
                                            <p class="text-white-50 mb-0">Total Message Sent</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="{{config('app.url')}}admin-assets/images/services-icon/03.png" alt="" >
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">Unit Left</h5>
                                            <h4 class="font-500">7000 <i class="mdi mdi-arrow-up text-success ml-2"></i></h4>
                                            <div class="mini-stat-label bg-info">
                                                <p class="mb-0"> 00%</p>
                                            </div>
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>
        
                                            <p class="text-white-50 mb-0">Total Unit Spent: 4000</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="{{config('app.url')}}admin-assets/images/services-icon/04.png" alt="" >
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">Birthdays Today</h5>
                                            <h4 class="font-500">{{$birthday_users->count()}}<i class="mdi mdi-arrow-up text-success ml-2"></i></h4>
                                           
                                            <div class="mini-stat-label bg-warning">
                                                <p class="mb-0"> + </p>
                                            </div>
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>
        
                                            <p class="text-white-50 mb-0">Total Message Templates</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        
                        <!-- end row -->

                        <div class="row">
                            <div class="col-xl-8">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="mt-0 header-title mb-4">Report: Recent Birthdays & Anniversaries</h4>
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                  <tr>
                                                    <th scope="col">(#) Id</th>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Phone</th>
                                                    <th scope="col" colspan="2"> SMS Status</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse($send_wishes as $send_wish)
                                                  <tr>
                                                    <th scope="row">{{$send_wish->id}}</th>
                                                    <td>
                                                        <div>
                                                            <img src="{{config('app.url')}}admin-assets/images/users/dunamis.png" alt="" class="thumb-md rounded-circle mr-2"> {{$send_wish->user->name}}
                                                        </div>
                                                    </td>
                                                    <td>{{$send_wish->user->birthday}} </td>
                                                    <td>{{$send_wish->user->phones}}</td>
                                                    <td><span class="badge badge-success">Delivered</span></td>
                                                  </tr>
                                                  <!-- <tr>
                                                    <th scope="row">#14257</th>
                                                    <td>
                                                        <div>
                                                            <img src="{{config('app.url')}}admin-assets/images/users/dunamis.png" alt="" class="thumb-md rounded-circle mr-2"> Pastor's Name
                                                        </div>
                                                    </td>
                                                    <td>17/03/2021</td>
                                                    <td>08123456947</td>
                                                    <td><span class="badge badge-success">Delivered</span></td>
                                                    <td>
                                                        <div>
                                                            <a href="#" class="btn btn-primary btn-sm"></a>
                                                        </div>
                                                    </td>
                                                  </tr> -->
                                                  

                                                  @empty
                                                <p> There is no recent birthdays</p>
                                                @endforelse
                                                

                                                  
                                                </tbody>
                                            
                                            </table>
                                            {{$send_wishes->links()}}
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- container-fluid -->

                </div>
@endsection
