<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('current_location')->nullable();
            $table->string('State')->nullable();
            $table->string('region')->nullable();
            $table->date('wed_anniversary')->nullable();
            $table->string('spouse_name')->nullable();
            $table->date('spouse_d_o_b')->nullable();
            $table->string('spouse_phone')->nullable();


            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
