<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     $name = 'Emmanuel';
//     return view('welcome',compact('name'));
// })->name('welcome');

Route::get('/','PagesController@home')->name('welcome');

Route::get('/about', 'PagesController@about')->name('about');

Route::get('/contact', 'PagesController@contact')->name('contact');
Route::get('/church-reg', 'PagesController@church_reg')->name('church-reg');
// Route::get('/register', 'PagesController@register')->name('register');
// Route::get('/login', 'PagesController@login')->name('login');

Route::get('/admin', 'AdminController@admin')->name('admin')->middleware('auth');
Route::get('/admin', 'AdminController@send_wishes')->name('admin')->middleware('auth');

Route::get('/branch', 'AdminController@branch')->name('branch')->middleware('auth');
Route::post('/branch', 'AdminController@add_branch')->name('add_branch')->middleware('auth');
Route::post('/update-branch', 'AdminController@update_branch')->name('update-branch')->middleware('auth');




Route::post('/church', 'AdminController@add_church')->name('add_church')->middleware('auth');
Route::get('/church','AdminController@church')->name('church')->middleware('auth');
Route::post('/update-church', 'AdminController@update_church')->name('update-church')->middleware('auth');


Route::get('church-branch/{slug}','AdminController@church_branch')->middleware('auth')->name('church-branch');


Route::get('/user/{user_type}', 'AdminController@user_type')->name('user_type')->middleware('auth');
Route::post('/user', 'AdminController@user')->name('user')->middleware('auth');

Route::post('/user', 'AdminController@add_user')->name('add_user')->middleware('auth');
Route::post('/update-user', 'AdminController@update_user')->name('update-user')->middleware('auth');


Route::get('/user', 'AdminController@user')->name('user')->middleware('auth');



Route::get('/home-cell', 'AdminController@home_cell')->name('home-cell')->middleware('auth');
Route::post('/home-cell', 'AdminController@add_home_cell')->name('add_home_cell')->middleware('auth');
Route::post('/update-home-cell', 'AdminController@update_home_cell')->name('update-home-cell')->middleware('auth');

Route::get('/sms-notify', 'AdminController@sms_notify')->name('sms-notify')->middleware('auth');










// Route::get('/about', function () {
//     return view('about');
// })->name('about');

// Route::get('/contact', function () {
//     return view('contact');
// })->name('contact');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
