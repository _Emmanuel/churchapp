<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserUser_type extends Model
{
    protected $fillable = ['user_id','user_type_id' ];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function user_type(){
        return $this->belongsTo(User_type::class, 'user_type_id');
    }

}
