<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'birthday', 'user_type', 'phones', 'current_location', 'State', 'region', 'wed_anniversary', 'spouse_name', 'spouse_d_o_b', 'spouse_phone'
    ];

    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function church_branch()
    // {
    //     $church_branch = \App\UserChurchBranch::where('user_id',$this->id)->first();
    //     return $church_branch;
    // }

    //for the sake of this, pastor would be ID 1, members would be ID 2 and so on
    public function user_type()
    {
        $user_type = 'Undefined';
        if ($this->user_type == 1) {
            $user_type = "Pastor";
        }
        if ($this->user_type == 2) {
            $user_type = "Member";
        }
        //As many user that we have we will loop them here so as to reduce our work on the frontend...
        return $user_type;
    }
    public function pastors()
    {
        $pastors = User::where('user_type', 1)->get();
        return $pastors;
    }
}
