<?php

namespace App\Jobs;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;

class SendSpouseBirthdayMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user;
    private $twilioClient;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $sid = config('services.twilio.account_sid');
        $token = config('services.twilio.auth_token');
        $this->twilioClient = new Client($sid, $token);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $template = "Blessing beloved Pst Mrs %s,I am excited to felicitate with you on this occasion of your birthday.The Lord shall give you a birthday gift to remember.'\n'As your days,so shall your strength be.You shall fulfill your days on earth,flourishing in the purpose of God for your life.Happy Birthday!";
        $body = sprintf($template, $this->user->spouse_name);
        $message = $this->twilioClient->messages->create(
            $this->user->spouse_phone,
            [
                'from' =>'Dr Enenche',
                'body' =>$body
            ]

        );
        Log::info($message->sid);
    }
}
