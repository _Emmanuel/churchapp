<?php

namespace App\Jobs;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;


class SendBirthdayMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user;
    private $twilioClient;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $sid = config('services.twilio.account_sid');
        $token = config('services.twilio.auth_token');
        $this->twilioClient = new Client($sid, $token);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $template = "Blessing beloved Pst %s,I am excited to felicitate with you on this occasion of your birthday.The Lord shall give you a birthday gift to remember.'\n'As your days,so shall your strength be.You shall fulfill your days on earth,flourishing in the purpose of God for your life.Happy Birthday!";
        //$template = "Blessing beloved Pst Mrs %s,I am excited to felicitate with you on this occasion of your birthday.The Lord shall give you a birthday gift to remember.'\n'As your days,so shall your strength be.You shall fulfill your days on earth,flourishing in the purpose of God for your life.Happy Birthday!";
        //$template = "Blessing beloved Pst & Pst(Mrs) %s,I am glad to celebrate with you today as you mark another anniversary in your marriage.The Lord continue to bless your family,'\n'renew your marital wine and add beauty and colour to your union in this season and beyond.Happy Wedding Anniversary!";
        
        //$body = sprintf($template, $this->user->spouse_name);
        $body = sprintf($template, $this->user->name);
        $message = $this->twilioClient->messages->create(
            $this->user->phones,
            //$this->user->spouse_phone,
            [

                'from' =>'Dr Enenche',
                'body' =>$body
            ]

        );
        Log::info($message->sid);
    }
}

// $template = "Blessing beloved Pastor %s,I am excited to felicitate with you on this occasion of your birthday.The Lord shall give you a birthday gift to remember.'\n'As your days,so shall your strength be.You shall fulfill your days on earth,flourishing in the purpose of God for your life.Happy Birthday!";
