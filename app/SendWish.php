<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SendWish extends Model
{
    protected $fillable = ['user_id'];

    public function user()
    {

        return $this->belongsTo(User::class,'user_id');
    }
}
