<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\SendWish;
use Carbon\Carbon;
use EmailNotification;
use SMSNotification;
use App\Jobs\SendBirthdayMessage;
use App\Jobs\SendAnniversaryMessage;
use App\Jobs\SendSpouseBirthdayMessage;

class AutoBirthday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'birthdays:send-wish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto Send Birthday messages to Pastors';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        $today = Carbon::now()->format('Y-m-d');
        //foreach ($birthday as $birthdays)
        //$today_birthdays = new Carbon ($birthdays)->format('m-d');
        $birthday_users = User::where('birthday', '=', $today)->get();
        $spouse_users = User::where('spouse_d_o_b', '=', $today)->get();
        $wed_users = User::where('wed_anniversary', '=', $today)->get();

        //sending birthday to pastor
        if ($birthday_users) {
            foreach ($birthday_users as $user) {



                SendBirthdayMessage::dispatch($user);
                SendWish::create([
                    'user_id' => $user->id
                ]);
            }
        }

        // if ($spouse_users) {
        //     foreach ($spouse_users as $user) {
        //         SendSpouseBirthdayMessage::dispatch($user);
        //         SendWish::create([
        //             'user_id' => $user->id
        //         ]);
        //     }
        // }

        if ($wed_users) {
            foreach ($wed_users as $user) {
                SendAnniversaryMessage::dispatch($user);
                SendWish::create([
                    'user_id' => $user->id
                ]);
            }
        }


    }



    // public function handle()
    // {
    //     $all_pastors = User::where('user_type',1)->get();
    //     foreach($all_pastors as $pastor){
    //         $birthday = $pastor->birthday;
    //         $name = $pastor->name;
    //         $email = $pastor->email;
    //         $phone = $pastor->phones;
    //         //$spouse_phone = $pastor->spouse_phone

    //         //Check if his birthday is Today's day nad Month only...
    //         //write check code here

    //         if ($birthday == $today) {
    //             //send him an Email
    //             $message_type = EmailNotification::where('message_type','birthday')->first();
    //             $message = $email_message->message;
    //             //Send him SMS
    //         }

    //         //go aand sleep....

    //     }

    // }
}
