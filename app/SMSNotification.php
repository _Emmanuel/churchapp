<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SMSNotification extends Model
{
    protected $fillable = ['message_type','message'];
}
