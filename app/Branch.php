<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = ['name','address','location','date_founded','resident_pastor','phones','email','church_id'];

    public function church()
    {
        return $this->belongsTo(Church::class,'church_id');
    }
}
