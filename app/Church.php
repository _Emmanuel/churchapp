<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Church extends Model
{
    protected $fillable = ['name','address','location','founder','phones','email','date_founded','slug','is_active'];

    // public function branches()
    // {
    //     return $this->hasMany(Branches::class,'church_id');
    // }
}
