<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchHomeCell extends Model
{
    Protected $fillable = ['branch_id', 'cell_name', 'cell_location', 'cell_host', 'cell_leader', 'cell_address', 'cell_date_founded', 'phones','email'];

    public function branch()
    {
        return $this->belongsTo(Branch::class,'branch_id');
    }
}