<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsNotify extends Model
{
    protected $fillable = ['message_type','message'];
}
