<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Church;
use App\Branch;
use App\BranchHomeCell;
use App\User;
use App\User_type;
use App\UserUser_type;
use App\SendWish;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;
use App\SmsNotify;
use Illuminate\Support\Facades\Hash;



class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin()
    {
        //Alert::success('Success Title', 'Success Message');

        return view('admin-pages.admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function branch()
    {
        $branches = Branch::with('church')->get();
        $churches = Church::all();
        //dd($branches);
        return view('admin-pages.branch',compact('branches','churches'));
    }

    public function add_branch(Request $request)
    {
        $input = $request->all();
        $create = Branch::create($input);
        if ($create) {
            Alert::success('Success', 'Branch Added');
            return redirect()->back();
        } else {
            Alert::error('Oops', 'Something went wrong, try again!');
            return redirect()->back();
        }
    }

    public function update_branch(Request $request)
    {
        $input = $request->all();
        $branch = Branch::findorFail($request->branch_id);
        $update = $branch->update($input);
        if ($update) {
            Alert::success('Success', 'Branch Updated');
            return redirect()->back();
        }else{
            Alert::error('Oops', 'Something went wrong, try again!');
            return redirect()->back();
        }
    }



    public function church()
    {
        $churches = Church::all();
        return view('admin-pages.church',compact('churches'));
    }

    public function home_cell()
    {
        $home_cells = BranchHomeCell::with('branch')->get();
        $branches = Branch::all();
        //dd($branches);
        return view('admin-pages.home-cell',compact('home_cells','branches'));
    }



    public function add_home_cell(Request $request)
    {
        $input = $request->all();
        $create = BranchHomeCell::create($input);
        if ($create) {
            Alert::success('Success', 'Branch Added');
            return redirect()->back();
        } else {
            Alert::error('Oops', 'Something went wrong, try again!');
            return redirect()->back();
        }
    }

    public function update_home_cell(Request $request)
    {
        $input = $request->all();
        $home_cell = Home_cell::findorFail($request->cell_id);
        $update = $home_cell->update($input);
        if ($update) {
            Alert::success('Success', 'Home Cell Updated');
            return redirect()->back();
        }else{
            Alert::error('Oops', 'Something went wrong, try again!');
            return redirect()->back();
        }
    }



    public function add_church(Request $request){
        $input = $request->all();

        //Add slug!
        $input['slug'] = time();
        //dd($input);
        $create = Church::create($input);
        if ($create) {
            Alert::success('Success', 'Church Added');
            return redirect()->back();
        } else {
            Alert::error('Error', 'Something is not right, try again');
            return redirect()-back();
        }
    }

    public function update_church(Request $request)
    {
        $input = $request->all();
        $church = Church::findorFail($request->church_id);
        $update = $church->update($input);
        if ($update) {
            Alert::success('Success', 'Church Updated');
            return redirect()->back();
        }else{
            Alert::error('Oops', 'Something went wrong, try again!');
            return redirect()->back();
        }
    }

    public function church_branch($slug)
    {
        $church = Church::where('slug',$slug)->firstorFail();
        $branches = Branch::where('church_id',$church->id)->get();
        $churches = Church::all();
        dd($churches);
        //return view('admin-pages.church_branch',compact('church','branches','churches'));

    }

    public function register(){
        return view('admin-pages.register');
    }

    public function login(){
        return view('admin-pages.login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function user_type()
    {
        $users = User::with('user_type')->get();
        $user_types = User_type::all();


        //dd($branches);
       // return view('admin-pages.branch',compact('branches','churches'));
        return view('admin-pages.user', compact('users','user_types'));
    }

    public function user()
    {
        $users = User::all();
        $pastors = User::where('user_type',1)->get();
        //dd($pastors);

        return view('admin-pages.user',compact('users'));
    }

    public function send_wishes()
    {
        $users = User::all();
        $send_wish = SendWish::all();
        $send_wishes = SendWish::latest()->paginate(10);
        // $birthday = Carbon::parse('birthday')->format('d M Y'); //28 Apr 2018
        //$send_wishes = SendWish::with('user')->get();
        $today = Carbon::now()->format('Y-m-d');
        $birthday_users = User::where('birthday', '=', $today)->get();
        return view('admin-pages.admin',compact('send_wishes', 'send_wish', 'users', 'birthday_users'));
    }


    public function pastors()
    {
        $pastors = User::where('user_type',1)->get();
        return view('admin-pages.pastors',compact('pastors'));
    }

    public function add_user(Request $request){
        $input = $request->all();

        //Add slug!
        $input['slug'] = time();
        //dd($input);
        //'password' => Hash::make($data['password']),
        $input['password'] = Hash::make('user123');
    //dd($input);
        $create = User::create($input);
        if ($create) {
            Alert::success('Success', 'User Added');
            return redirect()->back();
        } else {
            Alert::error('Error', 'Something is not right, try again');
            return redirect()-back();
        }

    }

    public function update_user(Request $request)
    {
        $input = $request->all();
        $user = User::findorFail($request->user_id)->first();
        $update = $user->update($input);
        if ($update) {
            Alert::success('Success', 'User Profile Updated');
            return redirect()->back();
        }else{
            Alert::error('Oops', 'Something went wrong, try again!');
            return redirect()->back();
        }
    }






    public function sms_notify()
    {
        $sms_notifies = SMSNotify::all();

        return view('admin-pages.sms-notify',compact('sms_notifies'));

    }






    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
