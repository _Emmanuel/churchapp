<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home()
    {        
        $text = 'This is a test';
        $text2 = 'Continuation';
        return view('front-pages.welcome', compact('text', 'text2'));
    }

    public function about(){
        return view('front-pages.about');
    }

    public function contact(){
        return view('front-pages.contact');
    }
    public function register(){
        return view('front-pages.register');
    }

    public function login(){
        return view('front-pages.login');
    }

    public function church_reg(){
        return view('front-pages.church-reg');
    }

    // public function admin(){
    //     return view('admin-pages.admin');
    // }
}
