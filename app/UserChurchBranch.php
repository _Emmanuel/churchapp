<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserChurchBranch extends Model
{
    protected $fillable = ['user_id', 'church_id', 'branch_id'];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function church(){
        return $this->belongsTo(Church::class, 'church_id');
    }

    public function branch(){
        return $this->belongsTo(Branch::class, 'branch_id');
    }
}
